# gcps

Search for and connect to GCP instances via fuzzy file finder. Only supports GCP compute instances until further notice

**Note: This tool is intended for internal use with HDS, modifications will be required for external usage of this tool**

![alt-text](doc/doc.gif)

# Usage
Example Usage:

* **gcps** (No arguements) - Connect to GCP instances using fzf serverlist
* **gcps update** - Updates serverlist for fzf, via gcloud
* **gcps install** - Installs fzf in current user directory

# First run
fzf needs a list of servers to run against. gcps will not run until this exists. Run **'gcps update'** to update the local server list

If fzf is not installed, you will be asked to run **'gcps install'** to install it. Follow the prompts and source your .bashrc after installation

# Updates
Planning to add on-prem systems, GCVE and more
