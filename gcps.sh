#!/bin/bash
# gcps - Search and connect to GCP instances via fuzzy file finder
# cal.parr@hdsupply.com
# Requires https://github.com/junegunn/fzf

# global
serverlist=~/serverlist.txt

# install fzf
install()
{
    fzf=$(which fzf)
    if [[ -f $fzf ]]; then
    printf "fzf is already installed, quitting.\\n"
    exit
    else
    printf "installing fzf..\\n"
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    ~/.fzf/install
    printf "installed fzf.\\n"
    exit
    fi
}

# update serverlist
update()
{
    printf "Updating server list..\\n"
    printf "gcloud is very slow.. be patient\\n"
    rm -rf $serverlist
    for i in $(gcloud projects list | awk '{print $1}' | awk 'NR>1'); do gcloud compute instances list --project=$i; done | grep -v NAME > $serverlist
    hostcount=$(wc -l $serverlist | awk '{print $1 }') && printf "$hostcount servers in $serverlist\\n"
    printf "Updated $serverlist\\n"
    exit
}

if [ "$1" == "update" ]; then
update
fi
if [ "$1" == "install" ]; then
install
fi

# Search
if [ -f "$serverlist" ]
then
    :
else
    printf "$serverlist not found.\\n"
    printf "Please run 'gcps update' to update the server list\\n"
    exit
fi
fzf=$(which fzf)
if [[ -f $fzf ]];
        then
            set -e
            server="$(cat $serverlist | fzf | awk '{print $1 }')"
            printf "Opening session to $server\\n"
            ssh "$server"
else
    printf "fzf is missing, quitting. Please run gcps install\\n"
    exit
fi
